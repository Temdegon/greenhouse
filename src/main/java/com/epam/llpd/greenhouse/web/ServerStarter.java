package com.epam.llpd.greenhouse.web;

import com.epam.llpd.greenhouse.App;
import com.epam.llpd.greenhouse.Greenhouse;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;

import java.io.File;
import java.net.MalformedURLException;
import java.util.TimeZone;

/**
 * Starts web server with web controls for the Greenhouse.
 * @author Pavel_Vervenko
 */
public class ServerStarter {

    private static final String RES_PATH = "com/epam/llpd/greenhouse";
    public static final String PHOTO_DIR_PATH = "cam";
    public static final int PORT = 8080;
    static {
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+3:00"));
    }

    private Server jettyServer;

    public static void main(String[] args) throws Exception {
        System.setProperty("org.eclipse.jetty.util.log.DEBUG", "true");
        ServerStarter serverStrarter = new ServerStarter();
        serverStrarter.runServer();
    }
    private Greenhouse box;

    private void runServer() throws Exception {
        jettyServer = new Server(PORT);
        jettyServer.setHandler(createHandlers());
        jettyServer.start();
        addShutdownHook();
        jettyServer.join();
    }

    private ResourceHandler createMainResHandler() {
        ResourceHandler rh = new ResourceHandler();
        rh.setResourceBase(getResourcePath());
        return rh;
    }

    private ResourceHandler createCameraImagesHandler() throws MalformedURLException {
        ResourceHandler imgHandler = new ResourceHandler();
        File file = new File(PHOTO_DIR_PATH);
        imgHandler.setResourceBase(file.toURI().toURL().toExternalForm());
        return imgHandler;
    }

    private Greenhouse createGrowBoxModel() {
        return App.createGrowBox(App.isRaspberry());
    }

    private HandlerList createHandlers() throws MalformedURLException {
        HandlerList handlers = new HandlerList();
        box = createGrowBoxModel();
        handlers.setHandlers(new Handler[]{
            createMainResHandler(), createCameraImagesHandler(), new GreenhouseControlsHandler(box),
            new ConfigurationHandler(box)
        });
        return handlers;
    }

    private void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    jettyServer.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String getResourcePath() {
        return getClass().getClassLoader()
                .getResource(RES_PATH).toExternalForm();
    }

}
