package com.epam.llpd.greenhouse.web;

import com.epam.llpd.greenhouse.Greenhouse;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

/**
 *
 * @author Pavel_Vervenko
 */
public class GreenhouseControlsHandler extends AbstractHandler {

    private static final String PATH = "/control";
    private final Greenhouse box;
    private final ObjectMapper jsonMapper = new ObjectMapper();

    GreenhouseControlsHandler(Greenhouse newBox) {
        this.box = newBox;
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (target.startsWith("/status")) {
            jsonMapper.writeValue(response.getWriter(), box.getStatus());
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
        }
        if (target.startsWith("/time-lapse")) {
            jsonMapper.writeValue(response.getWriter(), box.getTamelapseImages());
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);            
        }
        if (target.startsWith(PATH)) {
            String id = request.getParameter("id");
            if (id.equals("camButton")) {
                response.getWriter().print(box.makePhoto());
            } else {
                try {
                    box.toggleDevice(id);
                } catch (Exception e) {
                    Logger.getLogger(GreenhouseControlsHandler.class.getName()).log(Level.SEVERE, null, e);
                }
            }
            response.setStatus(HttpServletResponse.SC_OK);
            baseRequest.setHandled(true);
        }

    }
}
