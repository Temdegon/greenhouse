**Greenhouse** 

«Java for Farmers»: Greenhouse monitoring and automation using Java + Raspberry Pi.

**Hardware:**

* Raspberry Pi
* DHT11 Digital Temperature Humidity Sensor Module for Arduino
* 4-Channel 5V Relay Module Expansion Board for Arduino
* Power Supply SQ0331-0014 12V, 60Wt (for LED strip)
* 12V/5V converter
* SD Card
* Cooler
* Web camera


**Software:**

* Java SE Embedded 7
* JNI/C (for DHT11 sensor access)
* pi4j
* Servlets
* Jetty
* HTML/JS